#pragma once
#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"


class Bob {
	float i, j, k;
	int x, y, z;

public:
	Bob() :i(1.0), j(2.0), k(3.0), x(4), y(5), z(6) {}
	
	//void set();

	void set(float i, float j, float k, int x, int y, int z);

	float geti();
	float getj();
	float getk();
	int getx();
	int gety();
	int getz();

	void Write(OutputMemoryStream& out);

	void Read(InputMemoryStream& in);
};
