#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"



#if _WIN32
#include <Windows.h>
#endif

#include "Bob.h"

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

	InputMemoryStream *in;
	OutputMemoryStream out;

	Bob* bob;

	bob->Write(out);

	int copyLen = out.GetLength();
	char* copyBuff = new char[copyLen];
	// Copy over the buffer
	memcpy(copyBuff, out.GetBufferPtr(), copyLen);
	// Create a new memory stream

	in = new InputMemoryStream(copyBuff, copyLen);


}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
